import { StrideLogo } from '@atlaskit/logo';
import styled from 'styled-components';

const StyledLogo = styled(StrideLogo).attrs({
  size: 'xlarge',
  textColor: ({ theme }) => theme.colors.N0,
  iconColor: ({ theme }) => theme.colors.N0
})`
  margin-bottom: 30px;
`;

export default StyledLogo;
