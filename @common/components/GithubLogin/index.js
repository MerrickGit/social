import React from 'react';
import styled from 'styled-components';
import GithubIcon from '@atlaskit/icon/glyph/bitbucket/branches';
import Button from '@atlaskit/button';

const onClick = () => {
  window.open(
    'https://github.com/login/oauth/authorize?scope=user:email&client_id=667cf320f5d1f42f5bac&redirect_uri=http://localhost:3000/oauth/index.html',
    'Auth',
    `width=400,
      height=400,
      menubar=no,
      toolbar=no,
      resizable=no,
      location=no
    `
  );
};

const GithubLogin = styled(Button).attrs({
  iconBefore: <GithubIcon />,
})`
  justify-content: center;
`;

export default () => (
  <GithubLogin onClick={onClick}>
    Log in with Github
  </GithubLogin>
);
