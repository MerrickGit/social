export default {
  B500: '#0747A6',
  B400: '#0052CC',
  B100: '#4C9AFF',
  B50: '#DEEBFF',
  N800: '#172B4D',
  N0: '#FFF',
  N40: '#DFE1E6',
  N30: '#EBECF0',
  N80: '#97A0AF'
};
