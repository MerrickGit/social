import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Logo from '@common/components/Logo';

const Background = styled.div`
  background-color: ${({ theme }) => theme.colors.B500};
  min-height: 100vh;
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
`;

const Header = styled.span`
  color: ${({ theme }) => theme.colors.B50};
  font-size: 24px;
  font-weight: 500;
`;

const Wrapper = styled.div`
  box-sizing: border-box;
  width: 400px;
  margin-top: 33px;
  background-color: ${({ theme }) => theme.colors.N0};
  border-radius: 3px;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 10px;
  padding: 48px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const AfterBox = styled.div`
  display: flex;
  width: 400px;
  justify-content: space-between;
  margin-top: 18px;
  padding: 0 48px;
  box-sizing: border-box;
`;

const PageForm = ({
  title, after: After, children
}) => (
  <Background>
    <Logo />
    <Header>
      {title}
    </Header>
    <Wrapper>
      {children}
    </Wrapper>
    {After && (
      <AfterBox>
        <After />
      </AfterBox>
    )}
  </Background>
);

PageForm.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
  after: PropTypes.element.isRequired
};

export default PageForm;
