import { dispatch } from '@front/store';

const userInit = payload => dispatch({
  type: 'UI.USER-INIT',
  payload
});

const INITIAL_STATE = {};

const userReducer = (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case 'UI.USER-INIT':
      return {
        ...state,
        ...payload
      };
    default: return state;
  }
};

export { userInit };
export default userReducer;
