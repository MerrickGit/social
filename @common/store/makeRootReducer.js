import { combineReducers } from 'redux';
/**
 * @memberOf @platf/core/store
 * @desc Функция для создания root reducer
 * @param {object} asyncReducers Набор редюсеров
 *
 * @return {Function} Основной reducer приложения
 */
const makeRootReducer = asyncReducers =>
  combineReducers({ ...asyncReducers });
export default makeRootReducer;