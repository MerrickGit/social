import {
  applyMiddleware,
  compose,
  createStore as createReduxStore
} from 'redux';
import makeRootReducer from './makeRootReducer';

const createStore = (reducers) => {
  const middleware = [];
  const enhancers = [];
  let composeEnhancers = compose;
  if (process.env.NODE_ENV !== 'production') {
    /* eslint-disable */
    if (typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function') {
      composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
    }
    /* eslint-enable */
  }
  return createReduxStore(
    makeRootReducer(reducers),
    composeEnhancers(applyMiddleware(...middleware), ...enhancers)
  );
};
export default createStore;
