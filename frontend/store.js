import createStore from '@common/store/createStore';
import user from '@common/reducers/user';

const reducers = {
  user
};

const store = createStore(reducers);
if (module.hot) {
  module.hot.accept('@common/store/makeRootReducer', () => {
    const makeReducer = require('@common/store/makeRootReducer').default;
    store.replaceReducer(makeReducer(reducers));
  });
}
export default store;
export const { dispatch } = store;
export const { getState } = store;
