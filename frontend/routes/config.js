import Auth from './Auth';
import Main from './Main';
import GithubAuth from './GithubAuth';

const config = [
  { path: '/github', component: GithubAuth },
  { path: '/auth', component: Auth },
  { path: '/', component: Main }
];

export default config;