import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, withRouter } from 'react-router-dom';
import config from './config';

class Root extends React.Component {
  componentDidMount() {
    if (!localStorage.accessToken) {
      const { history } = this.props;
      history.replace('/auth');
    }
  }

  render() {
    return (
      <Switch>
        {config.map(({ component, path }) => (
          <Route key={path} component={component} path={path} />
        ))}
      </Switch>
    );
  }
};

Root.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func
  }).isRequired
};

export default withRouter(Root);
