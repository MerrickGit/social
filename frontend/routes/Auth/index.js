import React from 'react';
import styled from 'styled-components';
import GithubLogin from '@common/components/GithubLogin';
import PageForm from '@common/layouts/PageForm';

const Divider = styled.span`
  color: ${({ theme }) => theme.colors.N80};
  text-transform: uppercase;
  font-size: 11px;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  &:before {
    content: ' ';
    display: block;
    background: ${({ theme }) => theme.colors.N80};
    height: 1px;
    width: 60px;
    margin-right: 10px;
  }
  &:after {
    content: ' ';
    display: block;
    background: ${({ theme }) => theme.colors.N80};
    height: 1px;
    width: 60px;
    margin-left: 10px;
  }
`;

const Auth = () => (
  <PageForm title="Log in to continue to stride">
    <GithubLogin />
    <Divider>
      or
    </Divider>
  </PageForm>
);

export default Auth;
