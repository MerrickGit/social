import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import colors from '@common/styles/colors';

import Routes from './routes/Root';
import store from './store';

const theme = { colors };

const App = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <Router>
        <Routes />
      </Router>
    </ThemeProvider>
  </Provider>
);

render(<App />, document.querySelector('#root'));
