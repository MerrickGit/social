import { parse } from 'querystring';
import axios from 'axios';

document.addEventListener('DOMContentLoaded', async () => {
  const search = window.location.search.replace('?', '');
  const query = parse(search);

  if (query.code) {
    let user;
    const githubUser = await axios.put('http://localhost:3000/api/v1/auth/github', {
      code: query.code
    }).then(res => res.data);

    if (githubUser.hasRegister) {
      user = await axios.put('http://localhost:3000/api/v1/auth', githubUser);
    } else {
      const registerUser = await axios
        .put('http://localhost:3000/api/v1/auth/register', githubUser)
        .then(res => res.data);
      user = await axios
        .put('http://localhost:3000/api/v1/auth', registerUser)
        .then(res => res.data);
      console.log(user);
    }


    // .then(res => window.opener.postMessage(userCommand(res), '*'))
    // .then(() => window.opener.postMessage(routerCommand('/'), '*'))
    // .then(() => window.close())
    // .catch(() => window.close());
  } else {
    // window.close();
  }
});
