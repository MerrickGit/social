const path = require('path');
const webpack = require('webpack');
const history = require('connect-history-api-fallback');
const convert = require('koa-connect');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const alias = require('./alias');
const { version } = require('../package');

module.exports = {
  mode: process.env.PRODUCTION
    ? 'production'
    : 'development',

  entry: [
    'babel-polyfill',
    path.resolve(__dirname, '../frontend/app.js'),
  ],

  output: {
    path: path.resolve(__dirname, '../.bin/frontend'),
    filename: `${version}_app.js`,
    // publicPath: 'frontend/'
  },

  resolve: { alias },
  context: path.resolve(__dirname, '../'),
  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: { loader: 'babel-loader' },
      },
      {
        exclude: /node_modules/,
        test: /\.graphql$/,
        use: [{ loader: 'graphql-import-loader' }]
      }
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': process.env.PRODUCTION
        ? JSON.stringify('production')
        : JSON.stringify('development')
    }),
    new HtmlWebpackPlugin({
      template: 'frontend/public/index.html',
      inject: 'body'
    })
  ]
};

module.exports.serve = {
  content: [__dirname],
  open: true,
  host: '0.0.0.0',
  add: (app) => {
    app.use(convert(history({})));
  },
};
