const { resolve } = require('path');

module.exports = {
  '@': resolve(__dirname, '../'),
  '@back': resolve(__dirname, '../backend'),
  '@front': resolve(__dirname, '../frontend'),
  '@common': resolve(__dirname, '../@common')
};
