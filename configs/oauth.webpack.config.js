const path = require('path');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const alias = require('./alias');

module.exports = {
  mode: 'production',

  entry: [
    'babel-polyfill',
    path.resolve(__dirname, '../OAuth/github.js'),
  ],

  output: {
    path: path.resolve(__dirname, '../backend/public/oauth'),
    filename: 'oauth.js'
  },

  resolve: { alias },
  context: path.resolve(__dirname, '../'),
  devtool: 'none',

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: { loader: 'babel-loader' },
      }
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new HtmlWebpackPlugin({
      template: 'frontend/public/index.html',
      inject: 'body'
    })
  ]
};
