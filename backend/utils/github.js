const axios = require('axios');
const querystring = require('querystring');

const getAccessByCode = code =>
  axios.post('https://github.com/login/oauth/access_token', {
    client_id: '667cf320f5d1f42f5bac',
    client_secret: '3272a7a31852839479ecaf9a336cd9aa63c116d2',
    code
  })
    .then(res => res.data)
    .then(data => querystring.parse(data));

const getUserByToken = access => axios({
  method: 'GET',
  url: 'https://api.github.com/user',
  headers: { Authorization: `${access.token_type} ${access.access_token}` }
}).then(res => res.data);

module.exports = { getAccessByCode, getUserByToken };
