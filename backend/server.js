const Koa = require('koa');
const mongoose = require('mongoose');
const body = require('koa-bodyparser');
const cors = require('@koa/cors');
const serve = require('koa-static');
const { routes, allowedMethods } = require('./routes');

const app = new Koa();

app.use(body());
app.use(async (ctx, next) => {
  ctx.app.db = await mongoose.connect(
    'mongodb://merrick_admin:abc123@ds020228.mlab.com:20228/front_test',
    { useNewUrlParser: true }
  );

  await next();
});

app.use(cors());
app.use(routes);
app.use(allowedMethods);
app.use(serve(`${__dirname}/public`));

app.use(async (ctx) => {
  ctx.body = 'Hello world';
});

app.listen(3000);
