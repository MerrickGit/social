const Router = require('koa-router');
const api_v1 = require('./api');

const router = new Router();

router.use('/api/v1', api_v1);

module.exports = {
  routes: router.routes(),
  allowedMethods: router.allowedMethods()
};
