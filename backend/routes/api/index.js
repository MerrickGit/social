const Router = require('koa-router');
const auth = require('../auth');

const router = new Router();

router.get('/', async (ctx) => {
  ctx.body = 'API is alive';
});

router.use('/auth', auth);

module.exports = router.routes();
