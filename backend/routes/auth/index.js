const Router = require('koa-router');
const jwt = require('jsonwebtoken');
const { getAccessByCode, getUserByToken } = require('../../utils/github');
const User = require('../../model/user');

const secret = 'user_access';
const router = new Router();

router.get('/', async (ctx) => {
  ctx.body = 'Auth API is alive';
});

router.put('/', async (ctx) => {
  const { id: userId, githubId } = ctx.request.body;
  const id = userId || githubId;

  const user = await User.findOne({ githubId: id });

  const refreshToken = jwt.sign({ userId: user._id }, secret);
  await User.updateOne({ githubId: id }, { refreshToken });

  const accessToken = jwt.sign({
    userId: user._id,
    githubId: id
  }, secret);

  user.refreshToken = refreshToken;
  user.accessToken = accessToken;

  ctx.body = user;
});

router.put('/register', async (ctx) => {
  const { id, avatar_url, bio, name } = ctx.request.body;
  const user = await User.findOne({ githubId: id });
  let createdUser;

  if (!user) {
    createdUser = new User({
      githubId: id,
      avatarUrl: avatar_url,
      status: bio,
      name
    });

    await createdUser.save();
  }

  ctx.body = createdUser;
});

router.put('/github', async (ctx) => {
  const { code } = ctx.request.body;

  const access = await getAccessByCode(code);
  const githubUser = await getUserByToken(access);
  const user = await User.findOne({ githubId: githubUser.id });

  ctx.body = {
    ...githubUser,
    hasRegister: Boolean(user)
  };
});

module.exports = router.routes();
