const mongoose = require('mongoose');

const { Schema } = mongoose;

const User = Schema({
  githubId: { type: Number, index: true },
  email: { type: String },
  name: { type: String },
  avatarUrl: { type: String },
  status: { type: String },
  refreshToken: { type: String },
  accessToken: { type: String }
});

module.exports = mongoose.model('User', User);
